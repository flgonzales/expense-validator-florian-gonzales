package org.isima.javapro.valid;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.io.FileUtils;
import org.apache.directory.api.ldap.model.cursor.EntryCursor;
import org.apache.directory.api.ldap.model.exception.LdapException;
import org.apache.directory.api.ldap.model.message.SearchScope;
import org.apache.directory.ldap.client.api.LdapConnection;
import org.apache.directory.ldap.client.api.LdapNetworkConnection;
import org.codehaus.jackson.map.ObjectMapper;
import org.isima.javapro.expense.Expense;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.summingDouble;

public class ExpenseValidator {

  private static final String proxyHost = "";
  private static final String proxyPort = "";

  private static String EXPENSE_FILE_URL = "https://raw.githubusercontent.com/anthoRx/td1/master/expenses.csv";

  private static String HISTORY_FILE = "/history/valid_expenses.json";
  private static String INVALID_HISTORY_FILE = "/history/invalid_expenses.json";


  private static boolean havePolicyAmount(Expense e) {
    Map<String, Double> policy = new HashMap<>();
    policy.put("hotel", 180.0);
    policy.put("repas", 35.0);
    policy.put("train", 200.0);
    policy.put("avion", 800.0);
    policy.put("boisson", 100.0);

    Double policyValue = policy.get(e.getCategory());
    return policyValue == null || policyValue >= e.getAmount();
  }

  public void validate() {
    System.setProperty("http.proxyHost", proxyHost);
    System.setProperty("http.proxyPort", proxyPort);

    System.setProperty("https.proxyHost", proxyHost);
    System.setProperty("https.proxyPort", proxyPort);

    List<Expense> invalidExpenses = new ArrayList<>();
    List<Expense> validExpenses = new ArrayList<>();
    String toFile = "/tmp/expenses.csv";

    try {
      FileUtils.copyURLToFile(new URL(EXPENSE_FILE_URL), new File(toFile), 10000, 10000);
    } catch (IOException e) {
      e.printStackTrace();
      String localExpenseFile = getClass().getClassLoader().getResource("expenses.csv").getFile();
      try {
        FileUtils.copyFile(new File(localExpenseFile), new File(toFile));
      } catch (IOException ex) {
        ex.printStackTrace();
      }
    }

    CSVFormat csvFormat = CSVFormat.DEFAULT
        .withHeader("id", "employee", "date", "category", "description", "amount")
        .withDelimiter(';')
        .withTrim();

    DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");

    try {
      BufferedReader reader = new BufferedReader(new FileReader(toFile));
      CSVParser csvParser = new CSVParser(reader, csvFormat);

      for (CSVRecord csvRec : csvParser) {
        if (!"id".equals(csvRec.get("id"))) {
          LocalDate date = LocalDate.parse(csvRec.get("date"), dateFormatter);
          double amount = Double.parseDouble(csvRec.get("amount"));

          Expense expense = new Expense(csvRec.get("id"), csvRec.get("employee"), date, csvRec.get("category"), csvRec.get("description"), amount);

          if (isEmployeeExists(expense.getEmployee()) && expense.getAmount() > 0 && expense.getAmount() < Double.MAX_VALUE && havePolicyAmount(expense))
            validExpenses.add(expense);
          else
            invalidExpenses.add(expense);
        }
      }
    } catch (IOException e) {
      e.printStackTrace();
    }


    Map<String, List<Expense>> expensesByEmployee = validExpenses.stream().collect(groupingBy(Expense::getEmployee));
    List<Expense> expensesToSave = new ArrayList<>();
    expensesByEmployee.forEach((k, v) -> {

      double limit = 1000;
      boolean repectMonthPolicy = validExpenses.stream()
          .collect(groupingBy(Expense::getMonth, summingDouble(Expense::getAmount)))
          .entrySet()
          .stream()
          .allMatch(entry -> entry.getValue() < limit);
      if (repectMonthPolicy)
        expensesToSave.addAll(v);
      else
        invalidExpenses.addAll(v);
    });


    expensesToSave.forEach(expense -> {
      ObjectMapper objectMapper = new ObjectMapper();

      try {
        String jsonString = objectMapper.writeValueAsString(expense);
        File historyFile = new File(HISTORY_FILE);
        if (!Files.exists(historyFile.getParentFile().toPath()))
          Files.createDirectories(historyFile.getParentFile().toPath());
        if (historyFile.exists())
          Files.write(historyFile.toPath(), Arrays.asList(jsonString), StandardOpenOption.APPEND);
        else
          Files.write(historyFile.toPath(), Arrays.asList(jsonString), StandardOpenOption.CREATE);
        System.out.println("Valid expense saved in : " + HISTORY_FILE + " file.");
      } catch (IOException e) {
        e.printStackTrace();
      }
    });

    invalidExpenses.forEach(expense -> {
      ObjectMapper objectMapper = new ObjectMapper();

      try {
        String jsonString = objectMapper.writeValueAsString(expense);
        File historyFile = new File(INVALID_HISTORY_FILE);
        if (!Files.exists(historyFile.getParentFile().toPath()))
          Files.createDirectories(historyFile.getParentFile().toPath());
        if (historyFile.exists())
          Files.write(historyFile.toPath(), Arrays.asList(jsonString), StandardOpenOption.APPEND);
        else
          Files.write(historyFile.toPath(), Arrays.asList(jsonString), StandardOpenOption.CREATE);
        System.out.println("Invalid expense saved in : " + INVALID_HISTORY_FILE + " file.");
      } catch (IOException e) {
        e.printStackTrace();
      }
    });
  }


  private boolean isEmployeeExists(String employee) {
    boolean isValid;

    try {
      LdapConnection connection = new LdapNetworkConnection("ldap.forumsys.com", 389);
      connection.connect();
      connection.bind("cn=read-only-admin,dc=example,dc=com", "password");

      EntryCursor cursor = connection.search("uid=" + employee + ",dc=example,dc=com", "(objectclass=inetOrgPerson)", SearchScope.SUBTREE);

      isValid = cursor.iterator().hasNext();
      connection.unBind();
      connection.close();
    } catch (LdapException | IOException e) {
      System.out.println("Connection ldap impossible");
      isValid = Arrays.asList("galieleo", "newton", "einstein").contains(employee);
    }

    return isValid;
  }

}
